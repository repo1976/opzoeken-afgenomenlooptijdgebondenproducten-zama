package nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.input;

import com.google.common.collect.ImmutableList;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import nl.anwb.integration.fuseutilities.mybatis_pooled_jdbc_data_source.JdbcConfiguration;
import nl.anwb.integration.fuseutilities.mybatis_pooled_jdbc_data_source.JdbcMockingUtils;
import nl.anwb.integration.fuseutilities.mybatis_pooled_jdbc_data_source.model.ColumnMetaData;
import nl.anwb.integration.fuseutilities.mybatis_pooled_jdbc_data_source.model.JdbcResultSet;
import nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.mediation.ContractenDAO;
import nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.mediation.MediationRoute;
import nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.utils.TestCase;
import nl.anwb.integration.utilities.fileutils.FileUtils;
import nl.anwb.integration.utilities.xmlutils.XmlUtils;
import nl.anwb.integration.xmlcomparator.XMLComparator;
import org.apache.camel.EndpointInject;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.model.ModelCamelContext;
import org.apache.camel.test.spring.CamelSpringRunner;
import org.apache.camel.test.spring.CamelTestContextBootstrapper;
import org.apache.camel.test.spring.MockEndpoints;
import org.apache.camel.test.spring.UseAdviceWith;
import org.assertj.core.api.WithAssertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.BootstrapWith;
import org.springframework.test.context.ContextConfiguration;
import org.w3c.dom.Document;
import org.yaml.snakeyaml.Yaml;

import javax.sql.DataSource;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;
import java.sql.Types;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.rainerhahnekamp.sneakythrow.Sneaky.sneaked;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(CamelSpringRunner.class)
@UseAdviceWith
@BootstrapWith(CamelTestContextBootstrapper.class)
@ContextConfiguration("file:src/main/webapp/WEB-INF/camel-context.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@MockEndpoints
@ActiveProfiles({"test", "unit-test"})
@Profile("test")
@Component
@Slf4j
public class InputRouteTest implements WithAssertions {

    public static final String ROUTE_XML = "direct:start_in_xml";

    /** static breadcrumb id used in test exchanges */
    public static final String BREADCRUMB_ID = "aa-bb-11-22";

    private static final Yaml YAML = new Yaml();

    private static CallableStatement callableStatement;

    private static DataSource dataSource;
    @Autowired
    protected ModelCamelContext camelContext;
    @EndpointInject(uri = "mock:opvraging")
    private MockEndpoint opvragingEndpoint;
    @Autowired
    private ProducerTemplate producerTemplate;

    public InputRouteTest() { }

    /**
     * @return a mocked data source to use during tests,
     * it is refreshed for every Test and a class variable so it can be used to force a ConnectionTimeout
     */
    @Bean
    @Profile("test")
    public static DataSource getDataSource() throws SQLException {
        dataSource = mock(DataSource.class);
         callableStatement = JdbcMockingUtils.mockCallableStatementWithoutUpdates(dataSource);
        return dataSource;
    }

    @Bean
    @Profile("test")
    public JdbcConfiguration createJdbcConfiguration() {
        return mock(JdbcConfiguration.class);
    }

    /**
     * @return the column meta data as used by JDBC
     */
    public static List<ColumnMetaData> getColumnMetaDataForSuccess() {
        return new ImmutableList.Builder<ColumnMetaData>()
            .add(ColumnMetaData.builder().index(1).fieldName("Contractnummer").fieldLabel("Contractnummer").javaClassType(String.class).sqlColumnType(Types.VARCHAR).build())
            .add(ColumnMetaData.builder().index(2).fieldName("ContractStatus").fieldLabel("ContractStatus").javaClassType(String.class).sqlColumnType(Types.VARCHAR).build())
            .add(ColumnMetaData.builder().index(3).fieldName("BeginDatumContract").fieldLabel("BeginDatumContract").javaClassType(String.class).sqlColumnType(Types.VARCHAR).build())
            .add(ColumnMetaData.builder().index(4).fieldName("EindDatumContract").fieldLabel("EindDatumContract").javaClassType(String.class).sqlColumnType(Types.VARCHAR).build())
            .add(ColumnMetaData.builder().index(5).fieldName("Kenteken").fieldLabel("Kenteken").javaClassType(String.class).sqlColumnType(Types.VARCHAR).build())
            .add(ColumnMetaData.builder().index(6).fieldName("VoertuigIdentificatieNummer").fieldLabel("VoertuigIdentificatieNummer").javaClassType(String.class).sqlColumnType(Types.VARCHAR).build())
            .add(ColumnMetaData.builder().index(7).fieldName("FrameNummer").fieldLabel("FrameNummer").javaClassType(String.class).sqlColumnType(Types.VARCHAR).build())
            .add(ColumnMetaData.builder().index(8).fieldName("VervoermiddelSoort").fieldLabel("VervoermiddelSoort").javaClassType(String.class).sqlColumnType(Types.VARCHAR).build())
            .add(ColumnMetaData.builder().index(9).fieldName("VervoermiddelStatus").fieldLabel("VervoermiddelStatus").javaClassType(String.class).sqlColumnType(Types.VARCHAR).build())
            .add(ColumnMetaData.builder().index(10).fieldName("VervoermiddelBeginDatum").fieldLabel("VervoermiddelBeginDatum").javaClassType(String.class).sqlColumnType(Types.VARCHAR).build())
            .add(ColumnMetaData.builder().index(11).fieldName("VervoermiddelEindDatum").fieldLabel("VervoermiddelEindDatum").javaClassType(String.class).sqlColumnType(Types.VARCHAR).build())
            .add(ColumnMetaData.builder().index(12).fieldName("RelatieId").fieldLabel("RelatieId").javaClassType(String.class).sqlColumnType(Types.VARCHAR).build())
            .build();
    }

    public static List<ColumnMetaData> getColumnMetaDataForError() {
        return new ImmutableList.Builder<ColumnMetaData>()
            .add(ColumnMetaData.builder().index(1).fieldName("Error_spcMessage").fieldLabel("Error_spcMessage").javaClassType(String.class).sqlColumnType(Types.VARCHAR).build())
            .build();
    }

    public static List<ColumnMetaData> getColumnMetaDataForConnectionTimeOut() {
        return new ImmutableList.Builder<ColumnMetaData>()
            .add(ColumnMetaData.builder().index(1).fieldName("ConnectionTimeout").fieldLabel("ConnectionTimeout").javaClassType(String.class).sqlColumnType(Types.VARCHAR).build())
            .build();
    }

    public static List<ColumnMetaData> getColumnMetaDataForReadTimeOut() {
        return new ImmutableList.Builder<ColumnMetaData>()
            .add(ColumnMetaData.builder().index(1).fieldName("ReadTimeout").fieldLabel("ReadTimeout").javaClassType(String.class).sqlColumnType(Types.VARCHAR).build())
            .build();
    }

    @Before
    public void before() throws Exception {
        camelContext.getRouteDefinition(InputRoute.ROUTE_IN_SOAP).adviceWith(camelContext, new AdviceWithRouteBuilder() {
            @Override
            public void configure() {
                replaceFromWith(ROUTE_XML);
            }
        });

        camelContext.getRouteDefinition(MediationRoute.ROUTE_CALLOUT_CONTRACTEN_FROM_ZAMA).adviceWith(camelContext, new AdviceWithRouteBuilder() {
            @Override
            public void configure() {
                weaveById(MediationRoute.MYBATIS_LOOKUP_CONTRACTEN_FROM_ZAMA).before().to("mock:opvraging");
            }
        });

        camelContext.start();
    }

    @Test
    public void case001_HappyFlowRelatieID() {
        sendBodyAndAssert(TestCase.builder()
            .directory("/fixtures/completeRouteTests/case001_HappyFlowRelatieID/")
            .inputEndpoint(ROUTE_XML)
            .inputRequest("request.xml")
            .expectedCalloutRequest("request_expected.yml")
            .columnMetaData(getColumnMetaDataForSuccess())
            .mockDataCalloutResponse("mock_response.yml")
            .expectedResponse("response_expected.xml")
            .build());
    }

    @Test
    public void case002_HappyFlowKenteken() {
        sendBodyAndAssert(TestCase.builder()
            .directory("/fixtures/completeRouteTests/case002_HappyFlowKenteken/")
            .inputEndpoint(ROUTE_XML)
            .inputRequest("request.xml")
            .expectedCalloutRequest("request_expected.yml")
            .columnMetaData(getColumnMetaDataForSuccess())
            .mockDataCalloutResponse("mock_response.yml")
            .expectedResponse("response_expected.xml")
            .build());
    }

    @Test
    public void case003_HappyFlowRelatieIdEnVoertuigId() {
        sendBodyAndAssert(TestCase.builder()
            .directory("/fixtures/completeRouteTests/case003_HappyFlowRelatieIdEnVoertuigId/")
            .inputEndpoint(ROUTE_XML)
            .inputRequest("request.xml")
            .expectedCalloutRequest("request_expected.yml")
            .columnMetaData(getColumnMetaDataForSuccess())
            .mockDataCalloutResponse("mock_response.yml")
            .expectedResponse("response_expected.xml")
            .build());
    }

    @Test
    public void case004_HappyFlowExternContractId() {
        sendBodyAndAssert(TestCase.builder()
            .directory("/fixtures/completeRouteTests/case004_HappyFlowExternContractId/")
            .inputEndpoint(ROUTE_XML)
            .inputRequest("request.xml")
            .columnMetaData(getColumnMetaDataForSuccess())
            .mockDataCalloutResponse("mock_response.yml")
            .expectedResponse("response_expected.xml")
            .build());
    }

    //this one not working. testframework is not prepared for this
//    @Test
//    public void case005_HappyFlowLeeg() throws SQLException {
//        sendBodyAndAssert(TestCase.builder()
//            .directory("/fixtures/completeRouteTests/case005_HappyFlowLeeg/")
//            .inputEndpoint(ROUTE_XML)
//            .inputRequest("request.xml")
//            //            .expectedCalloutRequest("request_expected.yml")
//            .columnMetaData(getColumnMetaDataForSuccess())
//            .mockDataCalloutResponse("mock_response.yml")
//            .expectedResponse("response_expected.xml")
//            .build());
//    }

    @Test
    public void case006_InvalidRequest() {
        sendBodyAndAssert(TestCase.builder()
            .directory("/fixtures/completeRouteTests/case006_InvalidRequest/")
            .inputEndpoint(ROUTE_XML)
            .inputRequest("request.xml")
            .expectedResponse("response_expected.xml")
            .build());
    }

    @Test
    public void case007_InvalidRequestElfProef() {
        sendBodyAndAssert(TestCase.builder()
            .directory("/fixtures/completeRouteTests/case007_InvalidRequestElfProef/")
            .inputEndpoint(ROUTE_XML)
            .inputRequest("request.xml")
            .expectedResponse("response_expected.xml")
            .build());
    }

    private JdbcResultSet<List<Map<String, Object>>> getResultSetFromFixture(String mockDataFile) {
        // Load the yaml file - containing an abstraction of the rows / columns as returned by the database

        return new JdbcResultSet<>(YAML.load(getClass().getResourceAsStream(mockDataFile)));
    }

    private void conditionallyAssertCalloutRequest(TestCase testCase) {
        // Read the expected callout request file
        Optional.ofNullable(readFileFromClassPath(testCase.getDirectory(), testCase.getExpectedCalloutRequest())).ifPresent(expectedCalloutRequest -> {
            // Expect exactly one message
            opvragingEndpoint.expectedMessageCount(1);
            // Deserialize the expected request
            ContractenDAO expectedRequest = YAML.loadAs(expectedCalloutRequest, ContractenDAO.class);
            // Get the actual request
            ContractenDAO actualRequest = opvragingEndpoint.getExchanges().get(0).getIn().getBody(ContractenDAO.class);
            // Compare the requests (excluding the output field)

            assertThat(actualRequest).isEqualToIgnoringGivenFields(expectedRequest, "contracten");
        });
    }

    private ResultSet resultSet;

    @SneakyThrows
    private void sendBodyAndAssert(TestCase testCase) {
        mockJdbcResultSet(testCase);

        // Load the request
        String request = readFileFromClassPath(testCase.getDirectory(), testCase.getInputRequest());

        // Send the request to the start of the route and receive the output
        Object actualResponse = producerTemplate.sendBodyAndHeader(camelContext.getEndpoint(testCase.getInputEndpoint()), ExchangePattern.InOut, request, Exchange.BREADCRUMB_ID, BREADCRUMB_ID);

        // Load the expected response
        String expectedResponse = readFileFromClassPath(testCase.getDirectory(), testCase.getExpectedResponse());

        // Load the expected response ignore specifications
        String expectedResponseIgnoreSpecifications = readFileFromClassPath(testCase.getDirectory(), testCase.getExpectedResponseIgnoreSpecifications());

        assertResponse(testCase.getInputEndpoint(), actualResponse, expectedResponse, expectedResponseIgnoreSpecifications);

        // Assert the callout request
         conditionallyAssertCalloutRequest(testCase);

    }

    private void assertResponse(String inputEndpoint, Object actualResponse, String expectedResponse, String expectedResponseIgnoreSpecifications) {
        if (ROUTE_XML.equals(inputEndpoint)) {
            assertXmlResponse(actualResponse, expectedResponse, expectedResponseIgnoreSpecifications);
        } else {
            throw new IllegalArgumentException(String.format("Don't know how to assert the response of inputEndpoint: '%s'", inputEndpoint));
        }
    }

    private void assertXmlResponse(Object actualResponse, String expectedResponse, String expectedResponseIgnoreSpecifications) {
        try {
            Document received = XmlUtils.buildDocumentFromString(Optional.ofNullable(actualResponse).map(Object::toString).orElse(""));
            Document expected = XmlUtils.buildDocumentFromString(expectedResponse);
            Document expectedIgnore = Optional.ofNullable(expectedResponseIgnoreSpecifications).map(sneaked(XmlUtils::buildDocumentFromString)).orElse(null);

            XMLComparator comparator = new XMLComparator();
            // If there are differences
            if (comparator.doCompare(received, expected, expectedIgnore)) {
                log.error(comparator.getDiff().toString());
                assertThat(actualResponse).isEqualTo(expectedResponse);
            }
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    /**
     * Prepares the mock so it redirects calls on the the ResultSet to the given fakeResultSet
     */
    public void mockJdbcResultSet(TestCase testCase) throws SQLException {
        // If we have no data to return
        if (testCase.getMockDataCalloutResponse() == null) {
            // then don't bother to mock anything
            return;
        }

        // Prepare the jdbc mock for the callout
        JdbcResultSet<List<Map<String, Object>>> resultSetFromFixture = getResultSetFromFixture(testCase.getDirectory() + testCase.getMockDataCalloutResponse());

        mockJdbcResultSet(resultSetFromFixture, testCase.getColumnMetaData(), testCase.getCalloutResult(),13);
    }

    /**
     * Prepares the mock so it redirects calls on the the ResultSet to the given fakeResultSet
     * and initiates connectiontineouts and readtimeouts
     *
     * @param fakeResultSet  The resultset that should be the startingpoint for the response
     * @param columnMetaData The columnmetadata for the fakeResultSet
     * @param calloutResult  Enum that specifies the behaviour of the callout (SUCCESS, CONNECTION_TIMEOUT, READ_TIMEOUT)
     * @throws SQLException The SQLException thrown when a CoNNECTION_TIMEOUT or READ_TIMEOUT should be faked
     */
    public void mockJdbcResultSet(JdbcResultSet<List<Map<String, Object>>> fakeResultSet, List<ColumnMetaData> columnMetaData, TestCase.CalloutResult calloutResult, Integer outPropertyPosition) throws SQLException {
        resultSet = mock(ResultSet.class);
        JdbcMockingUtils.mockDefaultResultSet(resultSet, fakeResultSet, columnMetaData);
        if (calloutResult == null) {
            calloutResult = TestCase.CalloutResult.SUCCESS;
        }

        switch (calloutResult) {
            case CONNECTION_TIMEOUT:
                when(dataSource.getConnection()).thenThrow(new SQLException("Could not open JDBC Connection for transaction; nested exception is " +
                    "org.apache.commons.dbcp.SQLNestedException: Cannot create PoolableConnectionFactory (IO Error: The Network Adapter could not establish the " +
                    "connection)"));
                break;
            case READ_TIMEOUT:
                // I am not 100% sure that this is the actual exception thrown -
                when(callableStatement.execute()).thenThrow(new SQLTimeoutException());
                break;
            default:
                // Redirect all "getString" invocations on the mock to the actual result set
                doAnswer(invocationOnMock -> fakeResultSet.getObject(invocationOnMock.getArgument(0))).when(resultSet).getString(any());

                doAnswer(invocationOnMock -> resultSet).when(callableStatement).getObject(outPropertyPosition);
                break;
        }
    }

    public String readFileFromClassPath(String directory, String file) {
        // Defensive check to prevent concatenating a null value
        return directory == null || file == null ? null : readFileFromClassPath(directory + file);
    }

    @SneakyThrows
    public String readFileFromClassPath(String url) {
        return FileUtils.getStringFromFile(getClass().getResource(url).getFile());
    }

}