package nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.utils;

import lombok.Builder;
import lombok.Getter;
import nl.anwb.integration.fuseutilities.mybatis_pooled_jdbc_data_source.model.ColumnMetaData;

import java.util.List;

@Getter
@Builder
public class TestCase {

  public enum CalloutResult {SUCCESS, CONNECTION_TIMEOUT, READ_TIMEOUT }

  String directory;
  String inputEndpoint;
  String inputRequest;
  String mockDataCalloutResponse;
  List<ColumnMetaData> columnMetaData;
  String expectedCalloutRequest;
  String expectedResponse;
  String expectedResponseIgnoreSpecifications;
  CalloutResult calloutResult= CalloutResult.SUCCESS;
}
