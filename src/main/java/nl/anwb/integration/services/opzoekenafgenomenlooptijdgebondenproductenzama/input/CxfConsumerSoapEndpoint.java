package nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.input;

import nl.anwb.integration.fuseutilities.camel_service_utilities.endpoints.CxfEndpointFactory;
import org.apache.camel.component.cxf.CxfEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration("cxfConsumerSoapEndpointFactory")
public class CxfConsumerSoapEndpoint {

    public static final String CXF_CONSUMER_ENDPOINT = "CxfConsumerSoapEndpoint";

    @Bean(CxfConsumerSoapEndpoint.CXF_CONSUMER_ENDPOINT)
    public CxfEndpoint createCxfSoapEndpoint(@Autowired CxfEndpointFactory cxfEndpointFactory) {
        String webserviceUri = "/centraal/icc/webservices/opzoeken_afgenomenlooptijdgebondenproducten_zama_01_01";
        String wsdlUri = "wsdl/opzoeken_afgenomenlooptijdgebondenproducten_zama-1.1.wsdl";

        return cxfEndpointFactory.createDefaultServerEndpoint(webserviceUri, wsdlUri);
    }

}