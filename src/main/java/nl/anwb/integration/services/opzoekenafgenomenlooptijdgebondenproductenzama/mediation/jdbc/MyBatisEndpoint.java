package nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.mediation.jdbc;

import nl.anwb.integration.fuseutilities.mybatis_pooled_jdbc_data_source.JdbcConfiguration;
import nl.anwb.integration.fuseutilities.mybatis_pooled_jdbc_data_source.SqlDataSourceFactory;
import org.apache.camel.component.mybatis.MyBatisComponent;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
@MapperScan(value = "nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.mediation.jdbc", sqlSessionFactoryRef = "sqlSessionFactory")
@Component
public class MyBatisEndpoint {

  @Bean("mybatis")
  public MyBatisComponent createMyBatis(@Qualifier("sqlSessionFactory") SqlSessionFactory sqlSessionFactory) {
    MyBatisComponent myBatisComponent = new MyBatisComponent();
    myBatisComponent.setSqlSessionFactory(sqlSessionFactory);
    myBatisComponent.setConfigurationUri("nl/anwb/integration/services/opzoekenafgenomenlooptijdgebondenproductenzama/mediation/jdbc/sql-map-config.xml");

    return myBatisComponent;
  }

  @Bean("sqlSessionFactory")
  public SqlSessionFactory createSqlDataSourceFactory(@Qualifier("sqlDataSourceFactory") SqlDataSourceFactory sqlDataSourceFactory,
      @Value("${jdbc.url}") String jdbcUrl,
      @Value("${jdbc.username}") String username,
      @Value("${jdbc.password}") String password,
      @Value("${jdbc.maxPoolSize}") Integer maxPoolSize) {

    JdbcConfiguration jdbcConfiguration = JdbcConfiguration.builder().jdbcUrl(jdbcUrl).jdbcUsername(username).jdbcPassword(password).maxPoolSize(maxPoolSize).build();
    return sqlDataSourceFactory.createOracleBasedSqlSessionFactory(jdbcConfiguration);
  }

}

