package nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.mediation.jdbc;

import nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.mediation.ContractenDAO;

import java.util.List;
import java.util.Map;

/**
 * MyBatis mapper interface which can be used as DAO
 *
 * The return type is NOT a class input which the parameters are specified, to prevent 1-on-1 coupling of the returned values
 * In this way the stored procedure can be changed (adding fields) without breaking the service
 */

public interface OpzoekenContractenZaMaMapper {

    List<Map<String, Object>> contracten(ContractenDAO ContractenDAO);

}