package nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.input;

import nl.anwb.integration.processor.EncryptAuthenticationProcessor;
import nl.anwb.integration.processor.XmlValidatorProcessor;
import nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.mediation.MediationRoute;
import nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.response.ResponseRoute;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

import static nl.anwb.integration.processor.XmlToJsonTransformerProcessor.CALLTYPE;
import static nl.anwb.integration.processor.XmlToJsonTransformerProcessor.CALLTYPE_SOAP;
import static nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.MainConstants.HEADER_DETAIL_VALIDATIONERROR;
import static nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.MainConstants.HEADER_STATUSURN;
import static nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.MainConstants.HEADER_STATUSURN_VALUE_VALIDATIE;
import static nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.MainConstants.PROPERTYNAME_ORIGINAL_REQUEST_STRING;

@Component
public class InputRoute extends RouteBuilder {

    public static final String ROUTE_IN_SOAP = "in_soaproute";
    public static final String ROUTE_ADDPADSTAP = "direct:padstaproute";
    public static final String ROUTE_VALIDATE = "direct:validateroute";


    public void configure() {

        //@formatter:off
        from("cxf:bean:" + CxfConsumerSoapEndpoint.CXF_CONSUMER_ENDPOINT).routeId(ROUTE_IN_SOAP)
            .setProperty(CALLTYPE, simple(CALLTYPE_SOAP))
            .process(new EncryptAuthenticationProcessor())
            .to(ROUTE_ADDPADSTAP)
        ;
        //@formatter:on

        //@formatter:off
        from(ROUTE_ADDPADSTAP).routeId(ROUTE_ADDPADSTAP)
            .to("xslt:xsl/add_necessary_elements.xsl?saxon=true")
            .to(ROUTE_VALIDATE)
        ;
        //@formatter:on

        //@formatter:off
        from(ROUTE_VALIDATE).routeId(ROUTE_VALIDATE)
            .setProperty(PROPERTYNAME_ORIGINAL_REQUEST_STRING, simple("${in.body}"))
            .doTry()
                .process(new XmlValidatorProcessor("/conf/service_config.xml"
                    , "/xsl/validate_incoming_request.xsl"
                    , "/xsl/create_error_response.xsl"
                    , null))
                .removeHeader(XmlValidatorProcessor.XMLVALIDATIONRESULTHEADER)
                .to(MediationRoute.ROUTE_CALLOUT)
            .doCatch(Exception.class)
                .setHeader(HEADER_DETAIL_VALIDATIONERROR, exceptionMessage())
                .setHeader(HEADER_STATUSURN, simple(HEADER_STATUSURN_VALUE_VALIDATIE))
                .to(ResponseRoute.ROUTE_ERROR_RESPONSE)
            .end()
        ;
        //@formatter:on

    }


}
