package nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.mediation;


import nl.anwb.integration.fuseutilities.camel_service_utilities.endpoints.ClearCxfOperationHeaders;
import nl.anwb.integration.fuseutilities.exceptionhandling.GenericExceptionHandler;
import nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.response.ResponseRoute;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

import static nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.MainConstants.HEADER_DETAIL_CALLOUT;
import static nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.MainConstants.HEADER_ERROR_CAUSING;
import static nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.MainConstants.HEADER_ERROR_CAUSING_VALUE;
import static nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.MainConstants.HEADER_STATUSURN;
import static nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.MainConstants.HEADER_STATUSURN_VALUE_DOELSYSTEEM;

@Component
public class MediationRoute extends RouteBuilder {

    public static final String ROUTE_CALLOUT = "direct:calloutEBS";
    public static final String ROUTE_CALLOUT_EXCEPTION = "direct:calloutEBSException";

    public static final String MYBATIS_LOOKUP_CONTRACTEN_FROM_ZAMA = "LOOKUP_CONTRACTEN_FROM_ZAMA";

    public static final String ROUTE_CALLOUT_CONTRACTEN_FROM_ZAMA = "direct:calloutlookupContractenFromZAMA";


    @Override
    public void configure() {
        onException(Exception.class).handled(new GenericExceptionHandler(HEADER_DETAIL_CALLOUT)).to(ROUTE_CALLOUT_EXCEPTION);

        //@formatter:off
        from(ROUTE_CALLOUT).routeId(ROUTE_CALLOUT)
            .process(ClearCxfOperationHeaders.PROCESSOR)
            .process(RequestToOpvragingProcessor.PROCESSOR)
            .to(ROUTE_CALLOUT_CONTRACTEN_FROM_ZAMA)
            .process(ResultMapToXmlProcessor.PROCESSOR)
            .to(ResponseRoute.ROUTE_SUCCESS_RESPONSE)
        ;
        //@formatter:on

        //@formatter:off
        from(ROUTE_CALLOUT_EXCEPTION).routeId(ROUTE_CALLOUT_EXCEPTION)
            .setHeader(HEADER_STATUSURN, simple(HEADER_STATUSURN_VALUE_DOELSYSTEEM))
            .setHeader(HEADER_ERROR_CAUSING, simple(HEADER_ERROR_CAUSING_VALUE))
            .to(ResponseRoute.ROUTE_ERROR_RESPONSE)
        ;
        //@formatter:on

        //@formatter:off
        from(ROUTE_CALLOUT_CONTRACTEN_FROM_ZAMA).routeId(ROUTE_CALLOUT_CONTRACTEN_FROM_ZAMA)
            .to("mybatis:getContracten?statementType=SelectOne").id(MYBATIS_LOOKUP_CONTRACTEN_FROM_ZAMA)
        ;


    }


}
