package nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.response;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

import static nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.MainConstants.HEADER_CMR_BASE_URL;

@Component
public class ResponseRoute extends RouteBuilder {

    public static final String ROUTE_SUCCESS_RESPONSE = "direct:successresponse";
    public static final String ROUTE_ERROR_RESPONSE = "direct:errorresponse";

    @Override
    public void configure() {
        //@formatter:off
        from(ROUTE_SUCCESS_RESPONSE).routeId(ROUTE_SUCCESS_RESPONSE)
            .setHeader(HEADER_CMR_BASE_URL, simple("${properties:code.map.repository.base.url}"))
            .to("xslt:xsl/create_success_response.xsl?saxon=true")
            .removeHeaders("*")
        ;
        //@formatter:on

        //@formatter:off
        from(ROUTE_ERROR_RESPONSE).routeId(ROUTE_ERROR_RESPONSE)
            .process(ResetOriginalMessageProcessor.PROCESSOR)
            .setHeader(HEADER_CMR_BASE_URL, simple("${properties:code.map.repository.base.url}"))
            .to("xslt:xsl/create_error_response.xsl?saxon=true")
            .removeHeaders("*")
        ;
        //@formatter:on
    }

}
