package nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.mediation;

import nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.MainConstants;
import nl.anwb.integration.utilities.xmlutils.SimpleNamespaceResolver;
import nl.anwb.integration.utilities.xmlutils.XmlUtils;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Node;



/**
 * Prepares the request towards EBS by using the relevant values from the webservice request
 */
@Component(RequestToOpvragingProcessor.PROCESSOR)
public class RequestToOpvragingProcessor implements Processor {

  public static final String PROCESSOR = "RequestToOpvragingProcessor";

  @Override
  public void process(Exchange exchange) {
    Document requestDocument = exchange.getIn().getBody(Document.class);
    ContractenDAO contractenDAO = mapXmlRequestDocument(requestDocument);
    exchange.getIn().setBody(contractenDAO);
  }

  public ContractenDAO mapXmlRequestDocument(Document requestDocument) {

    Node requestNode = requestDocument.getDocumentElement();
    SimpleNamespaceResolver resolver = new SimpleNamespaceResolver("req", MainConstants.NS_REQ);
    Node recordNode = XmlUtils.singleNodeXPathQuery(requestNode, "//req:OpzoekenAfgenomenLooptijdGebondenProductenZaMaRequest", resolver);

    String businessTransactieReferentie = XmlUtils.stringValueXPathQuery(recordNode, "req:Header/req:BusinessTransactieReferentie/text()", resolver);
    String bronSysteemNaam = XmlUtils.stringValueXPathQuery(recordNode, "req:Header/req:BronSysteem/req:BronSysteemNaam/text()", resolver);
    String bronSysteemReferentie = XmlUtils.stringValueXPathQuery(recordNode, "req:Header/req:BronSysteem/req:BronSysteemReferentie/text()", resolver);
    String beginDatum = XmlUtils.stringValueXPathQuery(recordNode, "req:SelectieProfiel/req:BeginDatum/text()", resolver);
    String eindDatum = XmlUtils.stringValueXPathQuery(recordNode, "req:SelectieProfiel/req:EindDatum/text()", resolver);
    String annuleringenOpvragen = XmlUtils.stringValueXPathQuery(recordNode, "req:SelectieProfiel/req:AnnuleringenOpvragen/text()", resolver);
    String relatieId = XmlUtils.stringValueXPathQuery(recordNode, "req:SelectieProfiel/req:RelatieGegevens/req:RelatieId/text()", resolver);
    String contractId = XmlUtils.stringValueXPathQuery(recordNode, "req:SelectieProfiel/req:ContractGegevens/req:ContractId/text()", resolver);
    String externContractId = XmlUtils.stringValueXPathQuery(recordNode, "req:SelectieProfiel/req:ExternContractGegevens/req:ExternContractId/text()", resolver);
    String kenteken = XmlUtils.stringValueXPathQuery(recordNode, "req:SelectieProfiel/req:GedektObject/req:Vervoermiddel/req:VervoermiddelGegevens/req:VervoermiddelRegistratie/req:Kenteken" +
        "/text()", resolver);
    String voertuigIdentificatieNummer = XmlUtils.stringValueXPathQuery(recordNode, "req:SelectieProfiel/req:GedektObject/req:Vervoermiddel/req:VervoermiddelGegevens/req:VervoermiddelRegistratie/req:VoertuigIdentificatieNummer/text()", resolver);
    String frameNummer = XmlUtils.stringValueXPathQuery(recordNode, "req:SelectieProfiel/req:GedektObject/req:Vervoermiddel/req:VervoermiddelGegevens/req:VervoermiddelRegistratie/req:FrameNummer/text()", resolver);

    return ContractenDAO
        .builder()
          .businessTransactieReferentie(businessTransactieReferentie)
          .bronSysteemNaam(bronSysteemNaam)
          .bronSysteemReferentie(bronSysteemReferentie)
          .beginDatum(beginDatum)
          .eindDatum(eindDatum)
          .annuleringenOpvragen(annuleringenOpvragen)
          .relatieId(relatieId)
          .contractId(contractId)
          .externContractId(externContractId)
          .kenteken(kenteken)
          .voertuigIdentificatieNummer(voertuigIdentificatieNummer)
          .frameNummer(frameNummer)
        .build();
  }
}