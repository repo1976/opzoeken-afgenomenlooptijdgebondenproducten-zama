package nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.mediation;

import nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.MainConstants;
import nl.anwb.integration.utilities.dbresponsegenerator.ResponseGenerator;
import nl.anwb.integration.utilities.xmlutils.XMLException;
import nl.anwb.integration.utilities.xmlutils.XmlUtils;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.ParserConfigurationException;
import java.util.List;
import java.util.Map;

@Component(ResultMapToXmlProcessor.PROCESSOR)
public class ResultMapToXmlProcessor implements Processor {

  public static final String PROCESSOR = "ResultMapToXmlProcessor";
  private static final String PROPERTY_ZAMA_RESPONSE = "ZaMaResponse";

  /**
   * Prepares the exchange for xslt transformation:
   * <ul>
   * <li>Places the original Requestmessage as a header on the exchange message so it is available after transformation</li>
   * <li>Turns the query result into a standard xml (ResultSet with Resultaat with Waarde elements)</li>
   * <li>Places the resulting xml as a cxfPayload as a body on the input-message of the exchange - ready for transformation</li>
   * </ul>
   */
  @Override
  public void process(Exchange exchange) throws XMLException, ParserConfigurationException {
    // Places the original Requestmessage as a HEADER on the exchange message as a Node
    String originalMessage = (String) exchange.getProperty(MainConstants.PROPERTYNAME_ORIGINAL_REQUEST_STRING);
    Document requestDoc = XmlUtils.buildDocumentFromString(originalMessage);
    Node request = requestDoc.getDocumentElement();
    exchange.getMessage().setHeader(MainConstants.PROPERTYNAME_ORIGINAL_REQUEST_NODE, request);
    exchange.setProperty(MainConstants.PROPERTYNAME_ORIGINAL_REQUEST_NODE, request);

    // capture null, empty response.
    ContractenDAO contractenDAO = exchange.getMessage().getBody(ContractenDAO.class);

    ResponseGenerator responseGenerator = new ResponseGenerator();
    List<Map<String, Object>> contractenList = contractenDAO.getContracten();


    Document docContracten = responseGenerator.addResultsToDccument(contractenList, "contracten", "ZaMa");

    exchange.setProperty(PROPERTY_ZAMA_RESPONSE, docContracten);

    exchange.getIn().setBody(request);
  }


}
