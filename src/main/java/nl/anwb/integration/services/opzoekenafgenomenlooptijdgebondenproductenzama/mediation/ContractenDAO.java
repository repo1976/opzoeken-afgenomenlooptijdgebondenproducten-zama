package nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.mediation;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ContractenDAO {
  
    /** IN parameters used input the request towards EBS */
    private String businessTransactieReferentie;
    private String bronSysteemNaam;
    private String bronSysteemReferentie;
    private String beginDatum;
    private String eindDatum;
    private String annuleringenOpvragen;
    private String relatieId;
    private String contractId;
    private String externContractId;
    private String kenteken;
    private String voertuigIdentificatieNummer;
    private String frameNummer;

    /** OUT parameter initialized during the response */
    private List<Map<String, Object>> contracten;


}
