package nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.response;

import nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama.MainConstants;
import nl.anwb.integration.utilities.xmlutils.XMLException;
import nl.anwb.integration.utilities.xmlutils.XmlUtils;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;


@Component(ResetOriginalMessageProcessor.PROCESSOR)
public class ResetOriginalMessageProcessor implements Processor {

    public static final String PROCESSOR = "ResetOriginalMessageProcessor";

    @Override
    public void process(Exchange exchange) throws XMLException {
        String originalMessage = exchange.getProperty(MainConstants.PROPERTYNAME_ORIGINAL_REQUEST_STRING, String.class);
        Document doc = XmlUtils.buildDocumentFromString(originalMessage);
        exchange.getIn().setBody(doc, Document.class);
    }
}
