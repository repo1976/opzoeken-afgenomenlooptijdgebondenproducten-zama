package nl.anwb.integration.services.opzoekenafgenomenlooptijdgebondenproductenzama;

public class MainConstants {

    public static final String NS_REQ = "http://anwb.nl/webservices/opzoeken_afgenomenlooptijdgebondenproducten_zama_request/1";
    public static final String NS_RESP = "http://anwb.nl/webservices/opzoeken_afgenomenlooptijdgebondenproducten_zama_response/1";
    public static final String HEADER_DETAIL_JSONERROR = "detail-jsontranslation-error";
    public static final String HEADER_DETAIL_VALIDATIONERROR = "detail-validation-error";
    public static final String HEADER_DETAIL_CALLOUT = "detail-error";
    public static final String HEADER_STATUSURN = "status-urn";
    public static final String HEADER_STATUSURN_VALUE_VALIDATIE = "urn:ValidatieFout";
    public static final String HEADER_STATUSURN_VALUE_DOELSYSTEEM = "urn:DoelSysteemFout";
    public static final String HEADER_ERROR_CAUSING = "from-system";
    public static final String HEADER_ERROR_CAUSING_VALUE = "LookupBaseDatafromEBS";
    public static final String HEADER_ERROR_CAUSING_VALUE_INTEGRATION = "IntegrationPlatform";
    public static final String HEADER_CMR_BASE_URL = "code_map_repository_base_url";
    public static final String PROPERTYNAME_ORIGINAL_REQUEST_STRING = "original-request-payload";
    public static final String PROPERTYNAME_ORIGINAL_REQUEST_NODE = "original-request-node";
    private MainConstants() {
        throw new IllegalStateException("Utility class");
    }

}
