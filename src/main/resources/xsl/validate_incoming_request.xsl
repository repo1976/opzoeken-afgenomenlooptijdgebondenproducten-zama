<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" exclude-result-prefixes="xs req error" extension-element-prefixes="elfproef"
				xmlns:xs="http://www.w3.org/2001/XMLSchema"
				xmlns:req="http://anwb.nl/webservices/opzoeken_afgenomenlooptijdgebondenproducten_zama_request/1"
				xmlns:error="http://anwb.nl/icc/error"
				xmlns:elfproef="java:nl.anwb.integration.elfproef.ANWBElfproef">

	<xsl:template match="/req:OpzoekenAfgenomenLooptijdGebondenProductenZaMaRequest">
		<xsl:variable name="relatieid" select="req:SelectieProfiel/req:RelatieGegevens/req:RelatieId"/>
		<xsl:variable name="zoekvraag">
			<xsl:value-of select="req:SelectieProfiel/req:RelatieGegevens/req:RelatieId"/>
			<xsl:value-of select="req:SelectieProfiel/req:ContractGegevens/req:ContractId"/>
			<xsl:value-of select="req:SelectieProfiel/req:ExternContractGegevens/req:ExternContractId"/>
			<xsl:value-of select="req:SelectieProfiel/req:GedektObject/req:Vervoermiddel/req:VervoermiddelGegevens/req:VervoermiddelRegistratie/req:Kenteken"/>
			<xsl:value-of select="req:SelectieProfiel/req:GedektObject/req:Vervoermiddel/req:VervoermiddelGegevens/req:VervoermiddelRegistratie/req:VoertuigIdentificatieNummer"/>
			<xsl:value-of select="req:SelectieProfiel/req:GedektObject/req:Vervoermiddel/req:VervoermiddelGegevens/req:VervoermiddelRegistratie/req:FrameNummer"/>
		</xsl:variable>

		<xsl:variable name="foutmeldingen">
			<xsl:if test="$relatieid!='' and elfproef:isValid($relatieid)=false()">
				<xsl:value-of select="concat('RelatieId [',  $relatieid,'] voldoet niet aan de elfproef.    ')"/>
			</xsl:if>
			<xsl:if test="not($zoekvraag > '')">
				<xsl:text>Op basis van de ingevulde gegevens kan geen zoekvraag worden geformuleerd.    </xsl:text>
			</xsl:if>
		</xsl:variable>

		<xsl:if test="count($foutmeldingen)>0 and $foutmeldingen!=''">
			<xsl:value-of select="error(xs:QName('error:error'), $foutmeldingen)"/>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
