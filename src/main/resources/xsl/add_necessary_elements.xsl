<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:req="http://anwb.nl/webservices/opzoeken_afgenomenlooptijdgebondenproducten_zama_request/1"
        xmlns:xs="http://www.w3.org/2001/XMLSchema">

    <xsl:import href="classpath:canonical-support-utilities.xsl"/>

   <xsl:variable name="prefix">
        <xsl:choose>
            <xsl:when test="contains(name(/*),':')">
                <xsl:value-of select="concat(substring-before(name(/*),':'),':')"/>
            </xsl:when>
            <xsl:otherwise/>
        </xsl:choose>
    </xsl:variable>

    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="node() | text()">
        <xsl:copy>
            <xsl:copy-of select="@*"/>
            <xsl:choose>
                <xsl:when test="local-name(.)='Header'">
                    <xsl:apply-templates mode="addpadstap">
                        <xsl:with-param name="stapToAdd">
                            <xsl:call-template name="getstap"/>
                        </xsl:with-param>
                    </xsl:apply-templates>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:copy>
    </xsl:template>

    <xsl:template name="getstap">
        <xsl:variable name="btr" select="//req:BusinessTransactieReferentie" />
        <xsl:variable name="uniekId" select="/req:OpzoekenAfgenomenLooptijdGebondenProductenZaMaRequest/req:SelectieProfiel/req:RelatieGegevens/req:RelatieId/text()"/>
        <xsl:value-of select="concat($btr,'@OpzoekenAfgenomenLooptijdGebondenProductenZaMa(', $uniekId, ')')" />
    </xsl:template>
</xsl:stylesheet>