<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" exclude-result-prefixes="#all" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
				xmlns:cmr="nl.anwb.centraal.icc.xslt.codemaprepository_unforgiving"
				xmlns:icc="nl.anwb.centraal.icc.xslt.customfunctions"
				xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
				xmlns:req="http://anwb.nl/webservices/opzoeken_afgenomenlooptijdgebondenproducten_zama_request/1"
				xmlns:resp="http://anwb.nl/webservices/opzoeken_afgenomenlooptijdgebondenproducten_zama_response/1">

	<xsl:import href="classpath:codemaprepository_forFuse_cached_unforgiving.xsl" />
	<xsl:import href="classpath:change_ns.xsl"/>


	<xsl:param name="ZaMaResponse" />
	<xsl:param name="ZaMaMVResponse" />

	<xsl:variable name="Response">
		<xsl:choose>
			<xsl:when test="$ZaMaResponse">
				<xsl:copy-of select="$ZaMaResponse" />
			</xsl:when>
			<xsl:otherwise>
				<xsl:copy-of select="$ZaMaMVResponse" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="ZaMa">
		<xsl:choose>
			<xsl:when test="$ZaMaResponse">
				<xsl:text>ZAMA</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>ZAMAMV</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<xsl:variable name="Original">
		<xsl:copy-of select="//req:OpzoekenAfgenomenLooptijdGebondenProductenZaMaRequest"/>
	</xsl:variable>

	<xsl:template match="req:OpzoekenAfgenomenLooptijdGebondenProductenZaMaRequest" mode="HEADERS">
		<xsl:apply-templates select="req:Header" mode="resp"/>

		<xsl:variable name="selectieprofiel">
			<resp:SelectieProfiel>
				<xsl:apply-templates select="req:SelectieProfiel/req:BeginDatum" mode="resp"/>
				<xsl:apply-templates select="req:SelectieProfiel/req:EindDatum" mode="resp"/>
				<xsl:apply-templates select="req:SelectieProfiel/req:Strikt" mode="resp"/>
				<xsl:apply-templates select="req:SelectieProfiel/req:AnnuleringenOpvragen" mode="resp"/>
			</resp:SelectieProfiel>
		</xsl:variable>
		<xsl:if test="$selectieprofiel/resp:SelectieProfiel/*">
			<xsl:copy-of select="$selectieprofiel"/>
		</xsl:if>
	</xsl:template>

	<xsl:template match="*" mode="resp">
		<xsl:apply-templates select="." mode="change-ns">
			<xsl:with-param name="output-prefix" select="'resp'"/>
			<xsl:with-param name="output-ns" select="'http://anwb.nl/webservices/opzoeken_afgenomenlooptijdgebondenproducten_zama_response/1'"/>
		</xsl:apply-templates>
	</xsl:template>

	<xsl:template match="/">
		<resp:OpzoekenAfgenomenLooptijdGebondenProductenZaMaResponse>
			<xsl:apply-templates select="//req:OpzoekenAfgenomenLooptijdGebondenProductenZaMaRequest" mode="HEADERS"/>

			<resp:CanoniekeStatusMelding>
				<resp:StatusCode>0</resp:StatusCode>
				<resp:StatusURN>urn:Succes</resp:StatusURN>
				<resp:StatusMelding>Succes</resp:StatusMelding>
			</resp:CanoniekeStatusMelding>

			<xsl:variable name="Contracten">
				<xsl:apply-templates select="$Response/XRMResponse/ResultaatSet"/>
			</xsl:variable>
			<xsl:copy-of select="$Contracten"/>

		</resp:OpzoekenAfgenomenLooptijdGebondenProductenZaMaResponse>
	</xsl:template>

	<xsl:template match="ResultaatSet">
			<resp:ResultaatSet>
				<resp:ResultaatSetVolledig>
					<xsl:text>true</xsl:text>
				</resp:ResultaatSetVolledig>
				<resp:AllesSuccesvolOpgehaald>
					<xsl:text>true</xsl:text>
				</resp:AllesSuccesvolOpgehaald>
				<resp:OnvolledigeResultatenWeggefilterd>
					<xsl:text>false</xsl:text>
				</resp:OnvolledigeResultatenWeggefilterd>
				<!--  Resultaat/Waarde[@naam='Error_spcMessage']!='No Data Found' -->
				<xsl:if test="(not(Resultaat/Waarde[@naam='Error_spcMessage']) or Resultaat/Waarde[@naam='Error_spcMessage']/text()='') and Resultaat">
					<xsl:apply-templates select="Resultaat" mode="METGEGEVENS" />
				</xsl:if>
			</resp:ResultaatSet>
	</xsl:template>

	<xsl:template match="Resultaat" mode="METGEGEVENS">
		<resp:Resultaat>
			<resp:Authoratief>
				<xsl:text>true</xsl:text>
			</resp:Authoratief>
			<resp:ContractGegevens>
				<xsl:if test="Waarde[@naam='ContractStatus' and not(@xsi:nil='true')]">
					<xsl:attribute name="Status">
						<xsl:value-of select="cmr:getSingleValueDefault('hlv','statuscontract_zama_cdm', Waarde[@naam='ContractStatus'], 'ONBEKEND')" />
					</xsl:attribute>
				</xsl:if>
				<resp:ContractId>
					<xsl:value-of select="Waarde[@naam='Contractnummer']" />
				</resp:ContractId>
				<resp:BeginDatumContract>
					<xsl:value-of select="substring(Waarde[@naam='BeginDatumContract'],1,10)" />
				</resp:BeginDatumContract>
				<xsl:if test="Waarde[@naam='EindDatumContract' and not(@xsi:nil='true')]">
					<resp:EindDatumContract>
						<xsl:value-of select="substring(Waarde[@naam='EindDatumContract'],1,10)" />
					</resp:EindDatumContract>
				</xsl:if>
				<xsl:variable name="VervoermiddelRegistratie" select="$Original/req:OpzoekenAfgenomenLooptijdGebondenProductenZaMaRequest/req:SelectieProfiel/req:GedektObject/req:Vervoermiddel/req:VervoermiddelGegevens/req:VervoermiddelRegistratie"/>
				<xsl:if test="($VervoermiddelRegistratie/req:FrameNummer > '' and Waarde[@naam='FrameNummer'] = $VervoermiddelRegistratie/req:FrameNummer) or ($VervoermiddelRegistratie/req:VoertuigIdentificatieNummer > '' and Waarde[@naam='VoertuigIdentificatieNummer'] = $VervoermiddelRegistratie/req:VoertuigIdentificatieNummer) or ($VervoermiddelRegistratie/req:Kenteken > '' and Waarde[@naam='Kenteken'] = icc:normalizeLicensePlate($VervoermiddelRegistratie/req:Kenteken, $VervoermiddelRegistratie/req:RegistratieLand))">
					<resp:GedektObject>
						<resp:Vervoermiddel>
							<resp:VervoermiddelGegevens>
								<xsl:variable name="VervoermiddelStatus" select="cmr:getSingleValue('hlv','statuscontract_zama_cdm', Waarde[@naam='VervoermiddelStatus'])"/>
								<xsl:if test="$VervoermiddelStatus > ''">
									<xsl:attribute name="Status">
										<xsl:value-of select="$VervoermiddelStatus"/>
									</xsl:attribute>
								</xsl:if>
								<xsl:attribute name="GeldigheidPeriodeBeginDatum">
									<xsl:value-of select="substring(Waarde[@naam='VervoermiddelBeginDatum']/text(),1,10)" />
								</xsl:attribute>
								<xsl:if test="exists(Waarde[@naam='VervoermiddelEindDatum']/text())">
									<xsl:attribute name="GeldigheidPeriodeEindDatum">
										<xsl:value-of select="substring(Waarde[@naam='VervoermiddelEindDatum']/text(),1,10)" />
									</xsl:attribute>
								</xsl:if>
								<xsl:if test="Waarde[@naam='VervoermiddelSoort' and not(@xsi:nil='true')]">
									<resp:VervoermiddelSoort>
										<xsl:value-of select="cmr:getSingleValueDefault('hlv','soortvervoermiddel_zama_cdm', Waarde[@naam='VervoermiddelSoort'], 'ONBEKEND')" />
									</resp:VervoermiddelSoort>
								</xsl:if>
								<resp:VervoermiddelRegistratie>
									<xsl:if test="Waarde[@naam='Kenteken' and not(@xsi:nil='true')]">
										<resp:Kenteken>
											<xsl:value-of select="Waarde[@naam='Kenteken']" />
										</resp:Kenteken>
									</xsl:if>
									<xsl:if test="Waarde[@naam='VoertuigIdentificatieNummer' and not(@xsi:nil='true')]">
										<resp:VoertuigIdentificatieNummer>
											<xsl:value-of select="Waarde[@naam='VoertuigIdentificatieNummer']" />
										</resp:VoertuigIdentificatieNummer>
									</xsl:if>
									<xsl:if test="Waarde[@naam='FrameNummer' and not(@xsi:nil='true')]">
										<resp:FrameNummer>
											<xsl:value-of select="Waarde[@naam='FrameNummer']" />
										</resp:FrameNummer>
									</xsl:if>
								</resp:VervoermiddelRegistratie>
							</resp:VervoermiddelGegevens>
						</resp:Vervoermiddel>
					</resp:GedektObject>
				</xsl:if>
			</resp:ContractGegevens>
			<xsl:if test="Waarde[@naam='RelatieId' and not(@xsi:nil='true')]">
				<resp:RelatieGegevens>
					<resp:RelatieId>
						<xsl:value-of select="Waarde[@naam='RelatieId']" />
					</resp:RelatieId>
				</resp:RelatieGegevens>
			</xsl:if>
		</resp:Resultaat>
	</xsl:template>
</xsl:stylesheet>
