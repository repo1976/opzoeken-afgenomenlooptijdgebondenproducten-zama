<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" exclude-result-prefixes="req icc"
                xmlns:req="http://anwb.nl/webservices/opzoeken_afgenomenlooptijdgebondenproducten_zama_request/1"
                xmlns:resp="http://anwb.nl/webservices/opzoeken_afgenomenlooptijdgebondenproducten_zama_response/1"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:icc="nl.anwb.centraal.icc.xslt.customfunctions">

	<xsl:import href="classpath:change_ns.xsl"/>
	<xsl:import href="classpath:canonical-support-utilities.xsl"/>

	<xsl:param name="detail-jsonerror" select="''"/>
	<xsl:param name="detail-validation-error" select="''"/>
	<xsl:param name="detail-error" select="''"/>
	<xsl:param name="breadcrumbId" select="''"/>
	<xsl:param name="status-urn" select="'urn:ValidatieFout'"/>
	<xsl:param name="from-system" select="'Unknown'"/>

	<xsl:template match="/">
		<resp:OpzoekenAfgenomenLooptijdGebondenProductenZaMaResponse>
			<xsl:variable name="prefix" select="'resp'"/>
			<xsl:variable name="output-ns" select="'http://anwb.nl/webservices/opvragen_financiele_schuldeis_response/1'"/>
			<xsl:apply-templates select="//req:OpzoekenAfgenomenLooptijdGebondenProductenZaMaRequest/*" mode="change-ns">
				<xsl:with-param name="output-prefix" select="$prefix"/>
				<xsl:with-param name="output-ns" select="$output-ns"/>
			</xsl:apply-templates>
			<resp:CanoniekeStatusMelding>
				<xsl:copy-of select="icc:statusfields($status-urn, $from-system, $prefix, $output-ns)"/>
				<xsl:copy-of select="icc:statusdetailmelding($detail-jsonerror, $prefix, $output-ns)"/>
				<xsl:copy-of select="icc:statusdetailmelding($detail-validation-error, $prefix, $output-ns)"/>
				<xsl:copy-of select="icc:statusdetailmelding($detail-error, $prefix, $output-ns)"/>
				<xsl:copy-of select="icc:statusdetailmelding(concat('traceernummer:', $breadcrumbId), $prefix, $output-ns)"/>
			</resp:CanoniekeStatusMelding>
		</resp:OpzoekenAfgenomenLooptijdGebondenProductenZaMaResponse>
	</xsl:template>
</xsl:stylesheet>